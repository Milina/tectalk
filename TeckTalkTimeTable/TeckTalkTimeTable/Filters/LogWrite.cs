﻿using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TeckTalkTimeTable.Filters
{
    public class LogWrite : ActionFilterAttribute
    {


        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception!=null)
            {
                // Step 1. Create configuration object
                LoggingConfiguration config = new LoggingConfiguration();

                // Step 2. Create targets and add them to the configuration
                FileTarget fileTarget = new FileTarget();
                config.AddTarget("file", fileTarget);

                // Step 3. Set target properties

                fileTarget.FileName = "${basedir}/Logs/LogFile.txt";
                fileTarget.Layout = "${date} ${message}";

                // Step 4. Define rules          

                LoggingRule rule2 = new LoggingRule("*", LogLevel.Debug, fileTarget);
                config.LoggingRules.Add(rule2);

                // Step 5. Activate the configuration

                LogManager.Configuration = config;
                base.OnActionExecuted(filterContext);

                Logger logger = LogManager.GetLogger("Example");

                string[] exes = filterContext.Exception.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                logger.Error(exes[0] + exes[1]);
            }
        }


    }
}