﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TeckTalkTimeTable.Models;

namespace TeckTalkTimeTable.Models
{
    [Table("Topics")]
    public class Topics
    {
        [Key]
        public int id { get; set; }
        public int userID { get; set; }
        public string topic { get; set; }      
        public DateTime date { get; set; }
        
    }
}