﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TeckTalkTimeTable.Models
{
    public class Question
    {
        public int QuestionID { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        public string Catogary { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [StringLength(maximumLength: 200, MinimumLength = 5)]
        [DisplayName("Question Title")]
        public string QuestionTitle { get; set; }
       
        [Required]
        [DataType(DataType.MultilineText)]
        [DisplayName("Questions to Discuss")]
        public string QuestionToAsk { get; set; }
       
        [DataType(DataType.DateTime)]
        [DisplayName("Created Date")]
        public DateTime CreatedDate { get; set; }
       
        [DataType(DataType.Text)]
        public string UserName { get; set; }
        [DisplayName("Likes for the Question")]
        public int? NoOfLikesforTheQuestion { get; set; }
    }
}