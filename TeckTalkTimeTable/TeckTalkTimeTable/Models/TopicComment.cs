﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TeckTalkTimeTable.Models
{
        [Table ("TopicComment")]
        public class TopicComment
        {
            [Key]
            public int id { get; set; }
            public string UserID { get; set; }
            public string topicId { get; set; }
            public string topicComment { get; set; }
        }
}