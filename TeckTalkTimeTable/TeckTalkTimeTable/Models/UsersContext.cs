﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TeckTalkTimeTable.Migrations;

namespace TeckTalkTimeTable.Models
{
    public class UsersContext : IUserContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer<UsersContext>(new MigrateDatabaseToLatestVersion<UsersContext, Configuration>());
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Topics> Topics { get; set; }
        public DbSet<TopicComment> TopicComment { get; set; }
        public DbSet<Question> Questions { get; set; }

        public override IQueryable<T> Query<T>() 
        {
            return Set<T>();
        }

        public override void Add<T>(T entity) 
        {
            Set<T>().Add(entity);
        }

        public override void Update<T>(T entity) 
        {
            Entry(entity).State = System.Data.EntityState.Modified;
        }

        public override void Remove<T>(T entity) 
        {
            Set<T>().Remove(entity);
        }
        
       
    }
}