﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace TeckTalkTimeTable.Models
{
    public abstract  class IUserContext : DbContext
    {
        public IUserContext(string connection)
            : base(connection)
        {
        }

        public abstract IQueryable<T> Query<T>() where T : class;
      public abstract void Add<T>(T entity) where T : class;
      public abstract void Update<T>(T entity) where T : class;
      public abstract void Remove<T>(T entity) where T : class;
        
       
    }
}
