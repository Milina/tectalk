namespace TeckTalkTimeTable.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TeckTalkTimeTable.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TeckTalkTimeTable.Models.UsersContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(TeckTalkTimeTable.Models.UsersContext context)
        {
            context.Topics.AddOrUpdate(r => r.topic,
                new Topics { topic = "Test 4", date= new DateTime(2013, 1, 18), userID=1},
                new Topics { topic = "Test 1", date= new DateTime(2013, 2, 20), userID = 1 },
                new Topics { topic = "Test 2", date= new DateTime(2013, 3, 2), userID = 1 },
                new Topics { topic = "Test 3", date= new DateTime(2013, 4, 5), userID = 1 });
        }
    }
}
