namespace TeckTalkTimeTable.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tes1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Topics", "rrddd");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Topics", "rrddd", c => c.String());
        }
    }
}
