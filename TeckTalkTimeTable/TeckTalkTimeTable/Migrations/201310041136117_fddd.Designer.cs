// <auto-generated />
namespace TeckTalkTimeTable.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class fddd : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(fddd));
        
        string IMigrationMetadata.Id
        {
            get { return "201310041136117_fddd"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
