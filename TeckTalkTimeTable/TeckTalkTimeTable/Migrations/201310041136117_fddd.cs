namespace TeckTalkTimeTable.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fddd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Topics",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        userID = c.Int(nullable: false),
                        topic = c.String(),
                        date = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.UserProfile", t => t.userID, cascadeDelete: true)
                .Index(t => t.userID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Topics", new[] { "userID" });
            DropForeignKey("dbo.Topics", "userID", "dbo.UserProfile");
            DropTable("dbo.Topics");
            DropTable("dbo.UserProfile");
        }
    }
}
