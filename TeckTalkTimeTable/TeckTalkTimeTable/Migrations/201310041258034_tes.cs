namespace TeckTalkTimeTable.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tes : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.Topics", "userID", "dbo.UserProfile", "UserId", cascadeDelete: true);
            CreateIndex("dbo.Topics", "userID");
            DropColumn("dbo.UserProfile", "testfild");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfile", "testfild", c => c.String());
            DropIndex("dbo.Topics", new[] { "userID" });
            DropForeignKey("dbo.Topics", "userID", "dbo.UserProfile");
        }
    }
}
