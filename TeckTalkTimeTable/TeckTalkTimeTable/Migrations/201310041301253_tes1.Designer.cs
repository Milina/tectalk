// <auto-generated />
namespace TeckTalkTimeTable.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class tes1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(tes1));
        
        string IMigrationMetadata.Id
        {
            get { return "201310041301253_tes1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
