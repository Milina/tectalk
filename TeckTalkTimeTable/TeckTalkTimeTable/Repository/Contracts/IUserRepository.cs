﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeckTalkTimeTable.Models;

namespace TeckTalkTimeTable.Repository.Contracts
{
    public interface IUserRepository
    {
        ICollection<UserProfile> getUsersHasUpComingTopic();
    }
}