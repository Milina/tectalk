﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeckTalkTimeTable.Models;
using TeckTalkTimeTable.Repository.Contracts;

namespace TeckTalkTimeTable.Repository
{
    public class TecTalkUow
    {
        private IUserContext _UsersContext;
        public TecTalkUow() {
            _UsersContext = new UsersContext();
        }
        public TecTalkUow(IUserContext UserContext)
        {
            _UsersContext = UserContext;
        }

        public IUserContext bace { get { return _UsersContext; } }
        public IUserRepository Users { get { return new UserRepository(_UsersContext); } }

       
        public void Commit()        {
            
            _UsersContext.SaveChanges();
        }

       
    }
}