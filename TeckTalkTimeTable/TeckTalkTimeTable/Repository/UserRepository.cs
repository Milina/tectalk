﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeckTalkTimeTable.Models;
using TeckTalkTimeTable.Repository.Contracts;

namespace TeckTalkTimeTable.Repository
{

    public class UserRepository : IUserRepository
    {
        private IUserContext db;
        public UserRepository(IUserContext context)
        {
            db = context;
        }
            

        public ICollection<UserProfile> getUsersHasUpComingTopic()
        {
            return db.Query<UserProfile>().Where(r => r.topics.Count > 0).ToList<UserProfile>();
        }

    }
}