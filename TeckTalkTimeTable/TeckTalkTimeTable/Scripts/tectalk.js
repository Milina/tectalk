﻿$(function () {
     
    var getPartial = function () {
        var $link = $(this);

        $.get($link.attr("href"), function (data) {
            $("#dynamic_content").html(data);            
        });
        return false;
    };

   
    $(".content-wrapper").on("click", "#homes", getPartial);
    $(".content-wrapper").on("click", "#add_topic", getPartial);
    $(".content-wrapper").on("click", "#edit_topic", getPartial);
    $(".content-wrapper").on("click", "#delete_topic", getPartial);
    
    $(".content-wrapper").on("click", "#add_question", getPartial);
    $(".content-wrapper").on("click", "#edit_question", getPartial); 
    $(".content-wrapper").on("click", "#delete_question", getPartial);

});