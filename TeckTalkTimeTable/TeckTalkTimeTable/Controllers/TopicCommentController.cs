﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using TeckTalkTimeTable.Models;

namespace TeckTalkTimeTable.Controllers
{
    public class TopicCommentController : Controller
    {
        
            private UsersContext db = new UsersContext();
            public ActionResult index()
            {
                var topicCommentData = db.TopicComment.ToList();
                return PartialView("_TopicCommentList", topicCommentData);
            }

            public ActionResult Create()
            {
                return PartialView("_Create");
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult Create(TopicComment comment)
            {
                if (ModelState.IsValid)
                {
                    db.TopicComment.Add(comment);
                    db.SaveChanges();
                    var updatedtopicComment = db.TopicComment.ToList<TopicComment>();

                    return PartialView("_TopicCommentList", updatedtopicComment);
                }

                return View(comment);
            }

            public ActionResult Edit(int id = 0)
            {
                TopicComment editComment = db.TopicComment.Find(id);
                if (editComment == null)
                {
                    return PartialView(HttpNotFound());
                }
                return PartialView("_Edit", editComment);
            }

            //
            // POST: /Topics/Edit/5

            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult Edit(TopicComment comment)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(comment).State = EntityState.Modified;
                    db.SaveChanges();
                    var updatedComments = db.TopicComment.ToList<TopicComment>();
                    return PartialView("_TopicCommentList", updatedComments);
                }
                return View(comment);
            }

            public ActionResult Delete(int id = 0)
            {
                TopicComment comment = db.TopicComment.Find(id);
                if (comment != null)
                {
                    db.TopicComment.Remove(comment);
                    db.SaveChanges();

                }
                var updatedComment = db.TopicComment.ToList<TopicComment>();
                return PartialView("_TopicCommentList", updatedComment);
            }
        }
    
}
