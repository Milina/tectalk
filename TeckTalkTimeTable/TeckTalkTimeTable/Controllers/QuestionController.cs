﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TeckTalkTimeTable.Models;

namespace TeckTalkTimeTable.Controllers
{
    public class QuestionController : Controller
    {
        //
        // GET: /Question/
    

        public ActionResult Index()
        {
            var db = new UsersContext();
            var question = db.Questions.ToList<Question>();

            return PartialView("_Index", question);
        }

        #region CreateQuestion
        [HttpGet]
        public ActionResult CreateQuestion()
        {
            
            var catagoryList = new SelectList(new[] { "Technical", "NonTechnical", "Other" });
            ViewBag.CategoryList = catagoryList;
            return PartialView("_CreateQuestion");
        }

        [HttpPost]
        public ActionResult CreateQuestion([Bind(Exclude = "NoOfLikesforTheQuestion")] Models.Question question)
        {
            question.CreatedDate = DateTime.Now;
            question.UserName = Membership.GetUser().UserName.ToString();

            if (ModelState.IsValid)
            {
                var db = new UsersContext();
                db.Questions.Add(question);
                db.SaveChanges();
                var questions = db.Questions.ToList<Question>();
                return PartialView("_Index", questions);
            }
            return CreateQuestion();
        }
        #endregion

        #region Edit
        /// <summary>
        /// get the selected item id and show edit form
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id = 0)
        {
            var catagoryList = new SelectList(new[] { "Technical", "NonTechnical", "Other" });
            ViewBag.CategoryList = catagoryList;
            var db = new UsersContext();
            Question question = db.Questions.Find(id);
            if (question == null)
            {
                return PartialView(HttpNotFound());
            }
            return PartialView("_Edit", question);
        }

       
        /// <summary>
        /// if the editeed datea is valid to the model Question 
        /// update the database
        /// </summary>
        /// <param name="question"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Question question)
        {
            question.CreatedDate = DateTime.Now;
            question.UserName = Membership.GetUser().UserName.ToString();
            if (ModelState.IsValid)
            {
                var db = new UsersContext();
                db.Entry(question).State = EntityState.Modified;
                db.SaveChanges();
                var updaQuestion = db.Questions.ToList<Question>();
                return PartialView("_Index", updaQuestion);
            }
            return PartialView("_Index");
        }
        #endregion

        #region Delete
        public ActionResult Delete(int id = 0)
        {
             var db = new UsersContext();
            Question question = db.Questions.Find(id);
            if (question != null)
            {
                db.Questions.Remove(question);
                db.SaveChanges();

            }
            var deleteRow = db.Questions.ToList<Question>();
            return PartialView("_Index", deleteRow);
        }
        #endregion
    }
}
