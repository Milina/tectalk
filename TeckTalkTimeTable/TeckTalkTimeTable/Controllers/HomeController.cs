﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TeckTalkTimeTable.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            ViewData.Add("Message", "Modify this template to jump-start your ASP.NET MVC application.");

            return View();
        }

        
       
    }
}
