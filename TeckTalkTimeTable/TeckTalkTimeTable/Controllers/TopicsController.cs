﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeckTalkTimeTable.Filters;
using TeckTalkTimeTable.Models;
using WebMatrix.WebData;
using TeckTalkTimeTable.Repository;

namespace TeckTalkTimeTable.Controllers
{
    [InitializeSimpleMembership] 
    public class TopicsController : Controller   {

        private TecTalkUow Uow = new TecTalkUow();
        
       
        //
        // GET: /Topics/
        [LogWrite]
        public ActionResult Index()
        {
            Console.Write("faceffsffff");
          return PartialView("_TopicList", Uow.Users.getUsersHasUpComingTopic());
          
        }
             
        //
        // GET: /Topics/Create

        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        // kat 
        // POST: /Topics/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Topics topics)
        {
            topics.userID = WebSecurity.CurrentUserId;
            if (ModelState.IsValid)
            {
                Uow.bace.Add<Topics>(topics);
                Uow.Commit();


                return PartialView("_TopicList", Uow.Users.getUsersHasUpComingTopic());
          
            }

            return PartialView("_Create",topics);
        }

        //
        // GET: /Topics/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Topics topics = Uow.bace.Query<Topics>().Single(r => r.id == id);
            if (topics == null)
            {
                return PartialView(HttpNotFound());
            }
            return PartialView("_Edit", topics);
        }

        //
        // POST: /Topics/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Topics topics)
        {
            if (ModelState.IsValid)
            {
                Uow.bace.Update<Topics>(topics);
                Uow.Commit();


                return PartialView("_TopicList", Uow.Users.getUsersHasUpComingTopic());
          
            }
            return View(topics);
        }

        //
        // GET: /Topics/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Topics topics = Uow.bace.Query<Topics>().Single(r => r.id == id);
            if (topics != null)
            {
                Uow.bace.Remove<Topics>(topics);
                Uow.Commit(); 
                
            }


            return PartialView("_TopicList", Uow.Users.getUsersHasUpComingTopic());
          
        }

        //
        // POST: /Topics/Delete/5

        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Topics topics = db.Topics.Find(id);
        //    db.Topics.Remove(topics);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //
        // GET: /Topics/Details/5

        public ActionResult Details(int id = 0)
        {
            Topics topics = Uow.bace.Query<Topics>().Single(r => r.id == id);
            if (topics == null)
            {
                return HttpNotFound();
            }
            return View(topics);
        }

        protected override void Dispose(bool disposing)
        {
            Uow.bace.Dispose();
            base.Dispose(disposing);
        }
    }
}