﻿using System.Web.Mvc;
using TeckTalkTimeTable.Controllers;
using NUnit.Framework;

namespace TeckTalkTimeTable.Tests.Controllers
{
   [TestFixture]
    public class HomeControllerTest
    {
        [Test]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Modify this template to jump-start your ASP.NET MVC application.", result.ViewBag.Message);
        }

        [Test]
        public void test()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Modify this template to jump-start your ASP.NET ssMVC application.", result.ViewBag.Message);
        }


    }
}
